import logging
import os
from collections import defaultdict
from datetime import datetime, timedelta
from enum import Enum

from flask import Flask, request
from flask.json import jsonify

app = Flask(__name__)

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger("callback_server")

ALERT_FACTOR = int(os.getenv("ALERT_FACTOR", {{cookiecutter.alert_factor}}))
ALERT_PING_LIMIT = int(os.getenv("ALERT_PING_LIMIT", {{cookiecutter.alert_ping_limit}}))
ALERT_TIMERANGE = os.getenv("ALERT_TIMERANGE", "{{cookiecutter.alert_timerange}}")


class Cleaners(Enum):
    MICROSECOND = "microsecond"
    SECOND = "second"
    MINUTE = "minute"
    HOUR = "hour"
    DAY = "day"


def clean_date(dt, kind=Cleaners.MINUTE):
    cleaned_date = dt
    for c in Cleaners:
        if kind is c:
            break
        cleaned_date = cleaned_date.replace(**{c.value: 0})
    return cleaned_date


def get_last_ping():
    try:
        mtime = os.path.getmtime("/mnt/logs/ping.log")
        return (datetime.now() - datetime.fromtimestamp(mtime)).seconds
    except OSError:
        return None


def get_next_date(dt, kind=Cleaners.MINUTE):
    return dt - timedelta(**{"{}s".format(kind.value): 1})


def get_stats(kind=Cleaners.MINUTE, max_date=None, min_date=None, name="test"):
    dd = defaultdict(int)
    cur_date = clean_date(datetime.now(), kind=kind)
    log_path = "/mnt/logs/motion_detected.{}.log".format(name)

    if not os.path.exists(log_path):
        return

    with open(log_path) as motion_log:
        for line in reversed(motion_log.readlines()):
            cleaned_date = clean_date(
                datetime.fromisoformat(line.strip()[:-6]),
                kind=kind,
            )

            if max_date and cleaned_date > max_date:
                continue

            while cur_date > cleaned_date:
                yield cur_date.isoformat(), dd[cur_date]
                cur_date = get_next_date(cur_date, kind=kind)

                if min_date and cur_date < min_date:
                    return

            dd[cur_date] += 1


def get_timerange_args(value):
    if not value[:-1].isdigit():
        return 2 * 60 * 60

    kind = value[-1]

    if kind == "s":
        return int(value[:-1])
    elif kind == "m":
        return int(value[:-1]) * 60
    elif kind == "h":
        return int(value[:-1]) * 60 * 60
    elif kind == "d":
        return int(value[:-1]) * 60 * 60 * 24
    elif value.isdigit():
        return int(value)
    else:
        return 2 * 60 * 60


@app.route("/motion_data")
def motion_data():
    format = request.args.get("format", "chart")

    if format not in ("chart", "json"):
        return {"invalid format": format}, 400

    name = request.args.get("name") or "test"

    max_date = request.args.get("max_date")
    if max_date:
        max_date = datetime.fromisoformat(max_date)
    else:
        max_date = datetime.now()

    min_date = request.args.get("min_date")
    if min_date:
        min_date = datetime.fromisoformat(min_date)
        timerange = (max_date - min_date).total_seconds()
    else:
        timerange = get_timerange_args(request.args.get("timerange") or "2h")
        min_date = timerange and (datetime.now() - timedelta(seconds=timerange))

    kind = request.args.get("kind")

    if kind:
        kind = Cleaners(kind)
    elif timerange <= 60 * 5:
        kind = Cleaners("second")
    elif timerange <= 3600 * 5:
        kind = Cleaners("minute")
    else:
        kind = Cleaners("hour")

    stats_gen = get_stats(
        kind=kind,
        max_date=max_date,
        min_date=min_date,
        name=name,
    )

    if format == "chart":
        return jsonify(list(reversed(list(stats_gen)))), 200
    else:
        return {d: c for d, c in stats_gen}, 200


@app.route("/status")
def status():
    timerange = get_timerange_args(request.args.get("timerange") or ALERT_TIMERANGE)

    if timerange < 60:
        kind = Cleaners("second")
    elif timerange < 3600:
        kind = Cleaners("minute")
    else:
        kind = Cleaners("hour")

    min_date = datetime.now() - timedelta(seconds=timerange)

    factor = int(request.args.get("factor", ALERT_FACTOR))
    threshold = (timerange / 60.0) * factor

    count = sum(
        c
        for _, c in get_stats(
            kind=kind,
            min_date=min_date,
            name=request.args.get("name", "test"),
        )
    )

    last_ping = get_last_ping()

    if count > threshold or last_ping > ALERT_PING_LIMIT:
        status_code = 500
    else:
        status_code = 200

    return {"count": count, "last_ping": last_ping}, status_code
