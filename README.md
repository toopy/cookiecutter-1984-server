# CookieCutter Template For 1984-Server Setup

## Generate

Generate the project tree:

```bash
$ cookiecutter gl:toopy/cookiecutter-1984-server
picam_user [?]: ?
```

## Install

Install files and start services

```bash
$ sudo make -C 1984-server install
# install requirements
...
systemctl start 1984-server
```

## Gotcha!

Video links:

```bash
$ xdg-open http://localhost:8080/hls.html?name=test
$ cvlc rtmp://localhost:1935/live/test
```

Generate VLC playlist:

```bash
$ docker exec 1984-server genxspf --name test
$ vlc /home/joe/.docker/volumes/1984-server/mnt/flv/test.xspf
```

Motion links:

```bash
$ xdg-open http://localhost:8080/motion.html?name=test
$ xdg-open http://localhost:8080/motion_data?name=test
```

Status link:

```bash
$ xdg-open http://localhost:8080/status?name=test
```

## LICENSE

MIT
